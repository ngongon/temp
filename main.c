#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>


#define SIZE 16
#define N 64

typedef union {
  float f;
  unsigned int i;
}foo;

int main(int argc, char** argv)
{
  int i, j;
  foo container;

  float flat[(N+1)*SIZE];
  float input_a[SIZE];
  float input_b[SIZE*N];
  float output_cpu[N];
  float output_fpga[N];

  // initialization
  FILE *fd;
  fd = fopen("./input.txt", "r");

  unsigned int a;
  i = 0;
  while ( !feof(fd) )
  {
    if (fscanf(fd, "%X", &a) != EOF)
    {
      container.i = a;
      //printf("%f %X\n", container.f, container.i);
      if (i < SIZE)
        input_a[i] = container.f;
      else
        input_b[i - SIZE] = container.f;

      flat[i] = container.f;
      //printf("%d, %f\n", i, container.f);
      i++;
    }
  }
  fclose(fd);
  for( j = 0 ; j < N ; j++){
	 output_cpu[j] = 0.0f;
  }
  // computation
  for(j = 0; j<N ; j++){
	  for (i = 0; i < SIZE; i++){
	    output_cpu[j] += input_a[SIZE-1-i] * input_b[(SIZE-1-i)+SIZE*(j)];
	}
 }
printf("done\n");


  // FPGA offloading
  // memory load
  int foo;
  foo = open("/dev/mem", O_RDWR | O_NONBLOCK);
  float *fpga_bram = mmap(NULL, ((N+1)*SIZE)* sizeof(float), PROT_WRITE, MAP_SHARED, foo, 0x40000000);
  for (i = 0; i < (N+1)*SIZE; i++)
  {
    *(fpga_bram + i) = flat[i];
  }

  // run
  unsigned int *fpga_ip = mmap(NULL, sizeof(float), PROT_WRITE, MAP_SHARED, foo, 0x43c00000);
  *fpga_ip = 0x5555;
  
  // wait
  while (*fpga_ip == 0x5555);

  // result //0~63 duplicate output fpga->index for loop
  for( j=0; j< N ; j++){
	  output_fpga[j] = *(fpga_bram+j);
  }


  // display
  printf("%-10s%-10s%-10s%-10s\n", "index", "CPU", "FPGA", "FPGA(hex)");
  for( j = 0 ; j< N ; j++){
  container.f = output_fpga[j];
  printf("%-10d%-10f%-10f%-10X\n", j, output_cpu[j], output_fpga[j], container.i);
  }

  close(foo);

  return 0;
}
