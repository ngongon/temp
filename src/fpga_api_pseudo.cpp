#include "fpga_api.h"
#include <stdio.h>
#include <cstring>
#define DATA_SIZE SIZE*(SIZE+1) // fpga bram data size

#define min(x,y) (((x)<(y))?(x):(y))

FPGA::FPGA(off_t data_addr, off_t api_addr)
{
    api_ = new unsigned int[SIZE];    // use api_ as tempolar output 
    data_ = new float[DATA_SIZE];	
}

FPGA::~FPGA()
{
    delete[] api_;
    delete[] data_;
}

float* FPGA::matrix(void)
{
	return data_ + SIZE;
}

float* FPGA::vector(void)
{
	return data_;
}

const float* FPGA::run()
{
    float* vec = this->vector();
    float* mat = this->matrix();
    float* out  = reinterpret_cast<float*>(api_);  

    for(int i = 0 ; i < SIZE; ++i)
    {
        out[i] = 0;

        for(int j = 0 ; j < SIZE; ++j)
           out[i] += vec[j] * mat[SIZE*i + j];
    }

	for(int i = 0 ; i < SIZE; ++i)
	{
		data_[i] = out[i];
	}

    return data_;    
}
//large_mat = 처음꺼 빼고
//input = 전체
//처음꺼 나머지를 곱해야됨
//smallMV()
void FPGA::largeMV(const float* large_mat, const float* input,
		float* output, int M, int N)
{
    float* vec = this->vector();
    float* mat = this->matrix();
 
//우선 64*64 나눈것 
	/*for(int i = 0 ; i < M*(N+1) ; ++i)
		flat[i] = ((float)rand()) / RAND_MAX;*/ 
	//initialize
	/*float* flat = new float[M*(N+1)];
	float* input = flat;
	float* mat = flat + M;  
	float* output = new float[N];
	float* output_fpga = new float[N];*/

    for (int i = 0; i < N; i++)	
    {
 	output[i] = 0.0f;
    }
    int input_cnt = M / 64;
    int output_cnt = N / 64;
    int N_sm, M_sm;
    for(int k = 0 ; k<=input_cnt;k++){
	M_sm =  (M-(k+1)*64 > 0 ) ? 64 : (M-k*64);
	for(int i = 0 ; i <=output_cnt;i++){
		N_sm = (N-(i+1)*64>0)?64:(N-i*64);
		for( int j = 0 ; j < N_sm ; j++){
			for( int l = 0; l <M_sm ; l++){
		output[i*64+j] += input[k*64+l] * large_mat[M*(i*64+j)+(k*64+l)];
	
		printf("%f\n", large_mat[M*(i*64+j)+(k*64+l)]);
		}
		}
	}
    }		
}
